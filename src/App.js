import React, { useEffect, useState } from "react";
import axios from "axios";
import "./App.css";
import moment from "moment"; // 자바스크립트영역에서 쓰려고 일반 모먼트 사용
import "moment/locale/ko";
import Moment from "react-moment"; //리액트 컴포넌트
import LoadingOverlay from "./components/LoadingOverlay/LoadingOverlay";
import DatePicker from "react-datepicker";
import { Gradient } from 'react-gradient';


import "./react-datepicker-cssmodules.css";
import "./react-datepicker.css";

// import { Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

moment.locale("ko");

function App() {
  function getCnt(data) {
    let cnt = 0;
    for (let i = 0; i < data.items.length; i++) {
      if (data.items[i].todo_done === false) {
        cnt++;
      }
    }
    return cnt;
  }

  function sortDesc(array, field) {
    array = array.sort(function (a, b) {
      a = new Date(a.field);
      b = new Date(b.field);
      return a > b ? -1 : a < b ? 1 : 0;
    });
  }

const gradients = [
    ['#7474BF','#348AC7']
];

  //todolist 상태값
  const [todolist, setTodolist] = useState({ items: [] });
  const [todoCount, setTodoCount] = useState(0);

  //Loading 상태값
  const [loading, setloading] = useState(true);

  //기본 값은 닫혀있는 상태
  const [open, toggleOpen] = useState(false);

  //백엔드
  const host = "http://localhost:5000";
  // const host = "https://a126751b7e04.ngrok.io";
  // const host = "https://node-todolist-wanju.cfapps.us10.hana.ondemand.com";

  //날짜 Date
  const [startDate, setStartDate] = useState(new Date());
  const TextInput = ({ value, onClick }) => (
    <button className="Textinput" onClick={onClick} >
      {value}
    </button>
  );

  //(e)는 이벤트 정보가 담겨있는 객체
  const handleOpen = (e) => {
    toggleOpen(!open); //open의 부정값. !open
  };

  // 메세지 전송 함수
  // let msg_type = 'C';
  const sendMessage = (user_id, msg_type, copydata) => {
    //C:CREATE, E:EDIT
    const options = {
      //만들어 둔 API 호출    메세지 전송 router
      url: `${host}/api/v1/todolist/message`,
      method: "POST",
      data: {
        user_id: user_id,
        msg_type: msg_type,
        copydata: copydata,
      },
    };
    return axios(options); //sendMessage함수가 호출되면 위 options에 따라 통신을 시도할 것
  };

  const handleBlur = (e) => {
    const id = e.currentTarget.id.split("-").reverse()[0];
    const copydata = { ...todolist };
    copydata.items[id].todo_title = e.target.value;

    //edit를 위한 속성값 임시 추가
    copydata.items[id].todo_edit = !copydata.items[id].todo_edit;

    setTodolist(copydata);

    setloading(true);

    updateTodolist(copydata)
      .then((response) => {
        if (response.data) {
          setTodolist(response.data);

          setloading(false);
        }
      })
      .catch((error) => {
        debugger;
        setloading(false);
      });
  };

  //앱 종료 (일반 브라우저에서는 막힌 기능. 카카오 워크에서만 작동)
  const handleCloseApp = (e) => {
    window.location.herf = "app://close";
  };

  // 키 입력 이벤트 정의
  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      const todoText = e.target.value; //타겟에 입력된 값
      const copydata = { ...todolist };
      const deadline = moment(startDate)
        .format("YYYY-MM-DD HH:mm:ss")
        .split(" ")[0]
        .replace(/-/gi, "-");
      
      //Todo 추가
      if (e.target.className === "create-input") {
        const item = {
          id: "2574150",
          todo_title: todoText,
          todo_done: false,
          createAt: moment().format("YYYY-MM-DD HH:mm:ss"), // momont() = new Date()
          modifiedAt: moment().format("YYYY-MM-DD HH:mm:ss"),
          deadline: deadline,
        };

        setloading(true);

        const msg_type = "C"; //Creat
        //비동기통신이므로 then
        sendMessage(copydata.user_id, msg_type, item)
          .then((response) => {
            //메세지 전송이 성공했을 경우에만 아래 코드 수행
            if (response.data) {
              // Router에서 받아온 response.data가 있어야 수행
              copydata.items.push(item); // 배열의 가장 마지막 요소에 하나를 추가한다. ( []에 하나 추가 )

              //비동기통신 호출
              updateTodolist(copydata)
                .then((response) => {
                  if (response.data) {
                    //날짜순으로 정렬
                    response.data.items = response.data.items.sort(function (
                      a,
                      b
                    ) {
                      a = new Date(a.deadline);
                      b = new Date(b.deadline);
                      return a < b ? -1 : a > b ? 1 : 0;
                    });

                    setTodolist(response.data);
                    setTodoCount(getCnt(response.data));

                    e.target.value = "";
                    toggleOpen(false); //할일 창 상태값 변경
                    setloading(false);
                  }
                })
                .catch((error) => {
                  setloading(false);
                });
            }
          })
          .catch((error) => {
            setloading(false);
          });

        // Todo 수정
      } else if (e.target.className === "edit") {
        setloading(true);

        const id = e.currentTarget.id.split("-").reverse()[0];
        copydata.items[id].todo_title = e.target.value;
        copydata.items[id].modifiedAt = moment().format("YYYY-MM-DD HH:mm:ss");

        const msg_type = "E"; //Edit

        sendMessage(copydata.user_id, msg_type, copydata.items[id])
          .then((response) => {
            if (response.data) {
              //비동기통신 호출(업데이트)
              updateTodolist(copydata)
                .then((response) => {
                  if (response.data) {
                    setTodolist(response.data);
                    setloading(false);
                  }
                })
                .catch((error) => {
                  setloading(false);
                });
            }
          })
          .catch((error) => {
            setloading(false);
          });
      } // else if
    } //enter if
  };
  //Edit 버튼 누른 경우
  const handleEdit = (e) => {
    const id = e.currentTarget.id.split("-").reverse()[0];
    const copydata = { ...todolist };
    copydata.items[id].todo_edit = !copydata.items[id].todo_edit;
    setloading(true);
    setTodolist(copydata); //DB값을 바꿀 필요는 없음
    setloading(false);

    console.log(
      moment(moment().format("YYYY-MM-DD")).diff("2021-04-10", "days")
    );
  };

  const handleDone = (e) => {
    // 클릭한 대상에 따라 다르게 작동
    // "split하고 뒤집은 다음에 첫번째 자리. = id숫자"
    // const id = e.target.id.split('-').reverse()[0];
    const id = e.currentTarget.id.split("-").reverse()[0];
    //상태값은직접 접근해서 바꾸면 안됨. 상태값 변경 함수로 해야함
    // todolist.items[id].todo_done = !todolist.items[id].todo_done; // bad

    //  const copydata = todolist;  // 주소값이 복사 (기본적으로 오브젝트), shallow copy
    //  const copydata = JSON.parse(JSON.stringify(todolist)); // deep copy 오브젝트 스트링화->다시 오브젝트 (ES5)
    const copydata = { ...todolist }; //(ES6)
    copydata.items[id].todo_done = !copydata.items[id].todo_done;

    copydata.items[id].modifiedAt = moment().format("YYYY-MM-DD HH:mm:ss");
    //카피한 데이터를 업데이트 펑션으로 전달

    setloading(true);
    updateTodolist(copydata).then((response) => {
      if (response.data) setTodolist(response.data);

      setTodoCount(getCnt(response.data));

      setloading(false);
    });
    //catch추가하기
    setTodolist(copydata); //copy한 데이터 변경
  };

  // 삭제 function
  const handleDelete = (e) => {
    const id = e.currentTarget.id.split("-").reverse()[0];
    const copydata = { ...todolist };

    // const Array = [];
    // Array.pop(); // pop: 배열의 가장 마지막을 제거
    // Array.splice(startIdx, range); // splice: 시작점부터 n개 제거 ex: 0,2 (1번째부터 2개)

    copydata.items.splice(id, 1); // copydata에서 1개 삭제

    setloading(true); //api호출 전에 loading
    updateTodolist(copydata)
      .then((response) => {
        if (response.data) {
          setTodolist(response.data);
          setTodoCount(getCnt(response.data));
        }
        setloading(false);

        // setTodoCount(response.data.items.length); } //데이터가 있으면 True
      })
      .catch((error) => {
        setloading(false);
      });
  };

  // 리스트를 가져올 비동기 통신
  const getTodolist = () => {
    //axios를 호출하기 위한 옵션값
    const options = {
      url: `${host}/api/v1/todolist`,
      method: "GET",
      headers: {
        user_id: "2574150",
      },
      // data: "", GET 함수라서 따로 데이터를 받아오진 않음
    };
    //axios 호출, 비동기형태 프로미스 형태로 리턴이 된다.
    //프로미스 상태 3가지: 시작전/ 처리성공 / 오류
    return axios(options);
  };

  //업데이트를 위한 비동기 통신
  const updateTodolist = (todolist) => {
    //edit 필드 삭제 후 저장
    todolist.items = todolist.items.map((item) => {
      delete item.todo_edit;
      return item;
    });

    sortDesc(todolist.items, "deadline");

    const options = {
      url: `${host}/api/v1/todolist`,
      method: "POST",
      headers: {
        "Content-Type": "application/json", //형식상으로 넣어주는 것
      },
      data: todolist, //todolist를 업데이트하겠다.
    };

    return axios(options);
  };
  //use Effect에서는 async await가 안됨.
  //then으로 리스폰스를 받아줌
  //상태값 변경은 useEffect
  useEffect(() => {
    getTodolist()
      .then((response) => {
        //response.data가 undefined가 아닌 경우에만 setTodolist
        if (response.data) {
          response.data.items = response.data.items.sort(function (a, b) {
            a = new Date(a.deadline);
            b = new Date(b.deadline);
            return a < b ? -1 : a > b ? 1 : 0;
          });

          setTodolist(response.data);

          let cnt = 0;
          for (let i = 0; i < response.data.items.length; i++) {
            if (response.data.items[i].todo_done === false) {
              cnt++;
            }
          }
          setTodoCount(cnt);
          setloading(false);
        }
      })

      .catch((error) => {
        setloading(false);
      });
  }, []);

  //====================================================RETURN====================================================/
  //render되는 부분
  return (
    //부모 div
    <div className="App">
      
      {/* //앱 종료 */}
      <div className="app-close" onClick={handleCloseApp}>
        <div className="fas fa-times"></div>
      </div>

      {/* 자바스크립트 문법을 사용햐기 위해 {} */}
      {loading && <LoadingOverlay />}
     
      <Gradient
        gradients={ gradients } // required
        property="background"
        duration={ 5000 }
        angle="45deg">

      <div className="todolist-top">
        <div className="toptext">
          
        <div className="toptext-msg"> {`안녕하세요, ${todolist.user_id}님`} </div>
          
          <div className="todolist-date">
            <Moment format="YYYY년 MM월 DD일">{new Date()}</Moment>
          </div>

          <div className="todolist-dayofweek">
            <Moment format="dddd">{new Date()}</Moment>
          </div>

        </div>
      </div>
      </Gradient>

      <div className="todolist-overview">{`일정 ${todoCount}개 남음`}</div>
      <div className="bottom">
        <div className="todolist-items">
          {/* firebase안에 있는 수 만큼 반복 */}
          {todolist.items.map((item, itemIdx) => {
            return (
              <div className="todolist-item" key={itemIdx}>
                {" "}
                {/*key 지정*/}
                <div className="todolist-item-content">
                  {/*class를 두개이상 쓰면 같은 태그는 뒤에꺼가 덮어씀 */}
                  {/* <div className="item-check item-done"> */}
                  {/* boolean으로 컨트롤 */}
                  {/* 부모에 이벤트를 주면 자식도 똑같이 이벤트가 먹음. */}
                  <div
                    id={`item-${itemIdx}`}
                    onClick={handleDone}
                    className={
                      item.todo_done ? "item-check item-done" : "item-check"
                    }
                  >
                    {item.todo_done && <i class="fas fa-check"></i>}
                    {/* {item.todo_done ? <i class="fas fa-check"></i> : null } 삼항연산자로 표현*/}
                  </div>

                  {/* done이 TRUE이면 text-done이고 아니면 item-text */}
                  {/* done일떄는 두개 다 적용해야하므로 클래스이름 2개 */}

                  {/* edit가 true이면 input 필드로 변경   */}
                  {item.todo_edit ? (
                    <div id={`edit-${itemIdx}`} className="item-text">
                      <input className="item-"
                        id={`edit-${itemIdx}`}
                        defaultValue={item.todo_title}
                        onKeyPress={handleKeyPress}
                        className="edit"
                        autoFocus
                        onBlur={handleBlur}
                      />
                    </div>
                  ) : (
                    // edit가 true가 아니면 input필드가 아님.
                    <div
                      className={
                        item.todo_done ? "item-text text-done" : "item-text"
                      }
                    >
                      {item.todo_title}
                    </div>
                  )}

                  <div className="item-menu">


                    <div id={`edit-${itemIdx}`} className="item-edit" onClick={handleEdit} >
                      <i class="fas fa-edit"></i>
                      {/* &#9999;&#65039; */}
                    </div>

                    <div
                      id={`delete-${itemIdx}`}
                      className="item-delete"
                      onClick={handleDelete}
                    >
                      <i class="fas fa-trash"></i>
                      {/* &#10060; */}
                    </div>
                  </div>
                </div>

                <div className="item-bottom">
                  <div id={`dday-${itemIdx}`} className="daylist">
                    {/* //현재날짜 - 데드라인이 0이면 D-DAY 출력 */}
                    {moment(moment().format("YYYY-MM-DD")).diff(
                      item.deadline,
                      "days"
                    ) === 0 ? (
                      <div className="dday-today"> D-DAY </div>
                    ) : moment(moment().format("YYYY-MM-DD")).diff(
                        item.deadline,
                        "days"
                      ) > 0 ? (
                      <div className="dday">
                        {" "}
                        {`D+${moment(moment().format("YYYY-MM-DD")).diff(
                          item.deadline,
                          "days"
                        )}`}{" "}
                      </div>
                    ) : (
                      <div className="dday">
                        {" "}
                        {`D${moment(moment().format("YYYY-MM-DD")).diff(
                          item.deadline,
                          "days"
                        )}`}{" "}
                      </div>
                    )}
                  </div>

                  <div className="item-deadline">{item.deadline}</div>
                </div>
              </div>
            );
          })}
        </div>

          {/* open 값이 true일때만 해당 div가 나타나도록 적용  */}
          {open && (
            <div className="bottom-sheet">

              <div className="bottom-item">
                <div className="setDate" >
                  <div className="icon"> <i class="far fa-calendar-check"> </i> </div>     
                  
                  <DatePicker
                    selected={startDate}
                    onChange={(date) => setStartDate(date)}
                    customInput={<TextInput />}
                    dateFormat="yyyy.MM.dd"
                    
                    />
                  
                </div>
              
                <div className="item-create">
                  <input className="create-input"
                    placeholder="날짜 선택 후 일정을 입력하세요."
                    onKeyPress={handleKeyPress}/>
                </div>
                
              </div>
            </div>
          )}

        {/* 버튼에 이벤트 지정 */}
        <div className="circle-button" onClick={handleOpen}>
          <i className="fas fa-plus"></i>
        </div>
      </div>          
    
    </div>


  );
}

export default App;
