import React from 'react';
import Loader from 'react-loader-spinner';
import "./LoadingOverlay.css"; //CSS파일 import

// LoadingOverlay 컴포넌트 생성
const LoadingOverlay = (props) => {
    return (<div className="Loading-overay">
            <Loader type={"Oval"} width={30} height={30} />
          </div>
          );
}

export default LoadingOverlay; //다른 곳에서 끌어다 쓸 수 있게 Export
